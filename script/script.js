const tabs = document.querySelectorAll('.tabs-title')
const tabContent = document.querySelectorAll('.tab-content');


function setActiveContent() {
    for (let i = 0; i < tabs.length; i++) {
        tabs[i].addEventListener('click', e => {
            let otherTabs = tabs[i].parentElement.children;
            for (let n = 0; n < otherTabs.length; n++) {
                if (otherTabs[n].classList.contains('active')) {
                    otherTabs[n].classList.remove('active')
                }
            }
            let activeTab = e.target.dataset.name;
            if (tabs[i].dataset.name === activeTab) {
                tabs[i].classList.add('active');
                tabContent.forEach(item => {
                    if (item.classList.contains('active')) {
                        item.classList.remove('active')
                    }
                    if(item.dataset.name === activeTab) {
                        item.classList.add('active')
                    }
                })
            }
        })
    }
}
setActiveContent()





